# Informações
# autor: "Aécio Pires"
# email: http://blog.aeciopires.com/contato
# Manifest: livro.pp
#
# Parameters: none
#
# Actions: Instala e configura os pacotes necessários a compilação de aplicaçoes
# através da integração contínua usando o Jenkins
#
# Sample Usage:
#
# sudo /opt/puppetlabs/bin/puppet apply livro.pp
#

# Declaração de variáveis
# Postfix
$smtp_server           = '127.0.0.1'
$smtp_port             = '25'
$smtp_banner           = 'carteiro'
$mynetworks            = "127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 ${::ipaddress}"
$relayhost             = ' '
$mydestination         = "localhost.localdomain localhost ${::hostname} ${::fqdn}"
$docker_installed      = 'YES'
$puppet_repo_installed = 'YES'
$postfix_service       = 'postfix'
$required_packages     = ['apt-transport-https',
  'ca-certificates',
  'curl',
  'software-properties-common',
  'wget',
  'acl',
  'sudo',
  'vim',
  'git',
  'maven',
  'build-essential',
  'libssl-dev',
  'gcc',
  'make',
  'openjdk-11-jdk',
  'unzip',
  'postfix',
  'mutt', ]

if $::operatingsystem == 'ubuntu' {

  case $::operatingsystemmajrelease {
    '22.04': {
      # Configurando o acesso ao repositório do Docker
      exec { 'install_docker':
        command  => 'true; \
          curl -fsSL https://get.docker.com -o get-docker.sh
          sh get-docker.sh ;
          echo "YES" > /tmp/.install_docker; ',
        onlyif   => "result=\"\$(cat /tmp/.install_docker;)\"; \
          test \"\$result\" != \"${docker_installed}\" ; ",
        provider => 'shell',
        path     => ['/usr/local/sbin', '/usr/local/bin','/usr/sbin','/usr/bin','/sbin','/bin'],
        timeout  => '14400', #tempo em segundos equivalente a 4 horas
        require  => Package[$required_packages],
      }
    }
    default: {
      # fail é uma função padrão do Puppet
      fail("[ERRO] Nao sei instalar os pacotes nesta distro.")
    }
  }
}
else{
  # fail é uma função padrão do Puppet
  fail("[ERRO] Nao sei instalar os pacotes nesta distro.")
}

# Criando o usuário livro
user { 'livro':
  ensure   => 'present',
  comment  => 'user livro,,,',
  groups   => ['livro', 'sudo', 'vagrant',],
  home     => '/home/livro',
  #A senha é: livro
  password => '$6$LyFI6Js1$/jopKYrVoUmfuGvfz54Q6eoZTyqf6X//WcGCQgdcqD919V..isRDr2dbCw2P2Z8V2mbIZ.miWavHu/GgRb9xC/',
  shell    => '/bin/bash',
  require  => [ Group['livro'],
    Group['sudo'],
    Package[$required_packages], ],
}

# Criando alguns grupos para o usuário livro
group { 'livro':
  ensure => present,
}

group { 'sudo':
  ensure => present,
}

# Criando o diretório HOME do usuário livro
file { '/home/livro':
  ensure  => directory,
  mode    => '0644',
  owner   => livro,
  group   => livro,
  require => User['livro'],
}

# Instalando os pacotes requeridos
package { $required_packages:
  ensure => present,
}

# Atualizando o pacote do Puppet-Agent e Puppet-Bolt
package { ['puppet-agent',
  'puppet-bolt']:
  ensure => latest,
}

# Adicionando no grupo docker os usuários vagrant e livro
exec { 'users_group_docker':
  command  => 'true; \
    usermod -aG docker vagrant; \
    setfacl -m user:vagrant:rw /var/run/docker.sock; \
    usermod -aG docker livro; \
    setfacl -m user:livro:rw /var/run/docker.sock; ',
  provider => 'shell',
  path     => ['/usr/local/sbin', '/usr/local/bin','/usr/sbin','/usr/bin','/sbin','/bin'],
  timeout  => '14400', #tempo em segundos equivalente a 4 horas
  require  => [ Exec['install_docker'],
    User['livro'], ],
}

# Iniciando o serviço Postfix
service { $postfix_service:
   ensure     => 'running',
   enable     => true,
   hasrestart => true,
   hasstatus  => true,
   require    => Package['postfix'],
 }

# Gerenciando o arquivo de configuração do Postfix
file { '/etc/postfix/main.cf':
  ensure  => 'file',
  notify  => Service[$postfix_service],
  #content => template("main.cf.erb"),
  mode    => '0744',
  owner   => 'root',
  group   => 'root',
  require => Package['postfix'],
}

# Adicionando registros no arquivo /etc/hosts
# para resolução de nome das VMs
host { 'ci-server.domain.com.br':
  ensure       => present,
  host_aliases => 'ci-server',
  ip           => '192.168.56.10',
}

host { 'node-ubuntu.domain.com.br':
  ensure       => present,
  host_aliases => 'node-ubuntu',
  ip           => '192.168.56.11',
}

host { 'prod.domain.com.br':
  ensure       => present,
  host_aliases => 'prod',
  ip           => '192.168.56.12',
}
